package ru.asychev.seleniumkotlin.manager

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.NoSuchElementException
import ru.asychev.seleniumkotlin.LoggableDriver
import java.io.File

class TestManagerImpl(private val driver: LoggableDriver) : TestManager {

    private val loginUrl = "https://id.rambler.ru/login-20/login"

    override fun openPage() {
        driver.apply {
            get(loginUrl)
            until { it.currentUrl == loginUrl }
        }
    }

    override fun auth(login: String, password: String) {
        driver.apply {
            assertLog(driver.currentUrl == loginUrl) { "Должна быть открыта страница входа" }
            findElement(By.id("login")).let {
                it.sendKeys(login)
                assertLog(it.getAttribute("value") == login) { "Логин не установлен" }
            }
            findElement(By.id("password")).sendKeys(password)
            findElement(By.className("rui-Button-content")).click()
            try {
                assertLog(!findElement(By.className("rui-FieldStatus-message")).isDisplayed) { "Неправильная почта или пароль" }
            } catch (e: NoSuchElementException) {
                until { findElement(By.className("rui__3otde")).isDisplayed }
                val inboxUrl = "https://mail.rambler.ru/folder/INBOX"
                get(inboxUrl)
                until { it.currentUrl == inboxUrl }
            }
        }
    }

    override fun openNewLetter() {
        driver.apply {
            findElement(By.className("rui-Button-content")).click()
            until { it.currentUrl == "https://mail.rambler.ru/compose" }
        }
    }

    override fun setTitle(title: String) {
        driver.apply {
            findElement(By.id("subject")).let {
                it.sendKeys(title)
                assertLog(it.getAttribute("value") == title) { "Тема письма не установлена" }
            }
        }
    }

    override fun setReceiver(receiver: String) {
        driver.apply {
            findElement(By.id("receivers")).let {
                it.sendKeys(receiver)
                assertLog(it.getAttribute("value") == receiver) { "Получатель письма не установлен" }
            }
        }
    }

    override fun sendLetter() {
        driver.apply {
            findElement(By.className("Compose-content-2V")).findElement(By.className("rui-Button-content")).click()
            until {
                findElement(By.className("SentLetter-head-1P")).isDisplayed
            }
        }
    }

    override fun setContent(from: String) {
        setFile(File("src/test/resources/referat.pdf"))
        setFile(File("src/test/resources/image.jpg"))
        driver.findElement(By.id("editor_ifr")).run {
            sendKeys(File("src/test/resources/description.txt").readText())
            sendKeys(Keys.RETURN)
            sendKeys(Keys.chord(Keys.CONTROL, "k"))
            driver.findElement(By.className("mce-textbox")).sendKeys("https://rambler.ru")
            driver.findElement(By.xpath("//*[@id=\"mceu_39-button\"]/span")).click()
            sendKeys(Keys.ENTER)
            sendKeys(from)
        }
    }

    private fun setFile(file: File) {
        driver.apply {
            findElement(By.className("Compose-content-2V"))
                .findElement(By.className("Compose-fileInput-dl"))
                .sendKeys(file.absolutePath)
            val item =
                findElements(By.className("UploadItem-item-mh")).find { it.text == file.name }
            assertLog(item != null) { "Файл не установлен" }
            if (item != null) {
                until {
                    item.findElement(By.className("UploadItem-content-2T"))
                        .findElement(By.className("UploadItem-type-3O")).isDisplayed
                }
            }
        }
    }
}