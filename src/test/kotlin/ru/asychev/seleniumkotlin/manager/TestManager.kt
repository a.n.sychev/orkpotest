package ru.asychev.seleniumkotlin.manager

interface TestManager {

    fun openPage()

    fun auth(login: String, password: String)

    fun openNewLetter()

    fun setTitle(title: String)

    fun setReceiver(receiver: String)

    fun sendLetter()

    fun setContent(from: String)

}