package ru.asychev.seleniumkotlin

import org.apache.commons.io.FileUtils
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.WebDriverWait
import java.io.Closeable
import java.io.File
import java.lang.Exception

class LoggableDriver(private val driver: WebDriver, timeout: Long) : WebDriver by driver {
    private val wait = WebDriverWait(driver, timeout)

    inline fun assertLog(value: Boolean, lazyMessage: () -> Any) {
        try {
            assert(value, lazyMessage)
        } catch (e: AssertionError) {
            logTest(e.stackTraceToString())
            throw e
        }
    }

    fun until(isTrue: (WebDriver) -> Boolean) {
        try {
            wait.until { isTrue.invoke(driver) }
        } catch (e: Exception) {
            logTest(e.stackTraceToString())
            throw e
        }
    }

    fun logTest(exception: String) {
        val path = "src/test/resources/log/"
        (path + "${System.currentTimeMillis()}/").let {
            saveException(it, exception)
            saveHtml(it)
            takeScreenshot(it)
        }
    }

    private fun saveException(path: String, exception: String) {
        val finalPath = File(path + "exception.txt")
        FileUtils.writeStringToFile(finalPath, exception)
    }

    private fun saveHtml(path: String) {
        val finalPath = File(path + "html.txt")
        FileUtils.writeStringToFile(finalPath, driver.pageSource)
    }

    private fun takeScreenshot(path: String) {
        val finalPath = File(path + "screenshot.png")
        val file = (driver as TakesScreenshot).getScreenshotAs(OutputType.FILE)
        FileUtils.copyFile(file, finalPath)
    }

}