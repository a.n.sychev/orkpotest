package ru.asychev.seleniumkotlin

import org.junit.After
import org.junit.Test
import org.junit.Before
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.PageFactory
import ru.asychev.seleniumkotlin.manager.TestManager
import ru.asychev.seleniumkotlin.manager.TestManagerImpl
import java.util.concurrent.TimeUnit

class RamblerMailTest {
    private val timeout = 10L
    private var driver = LoggableDriver(ChromeDriver(), timeout)
    private val testManger: TestManager = TestManagerImpl(driver)

    @Before
    fun initDriver() {
        PageFactory.initElements(driver, this)
        driver.manage()?.timeouts()?.implicitlyWait(timeout, TimeUnit.SECONDS)
        driver.manage()?.window()?.maximize()
    }

    @Test
    fun startTest() {
        val login = System.getProperty("login")
        val password = System.getProperty("password")
        val title = System.getProperty("title")
        val receiver = System.getProperty("receiver")
        val from = System.getProperty("from")
        testManger.openPage()
        testManger.auth(login, password)
        testManger.openNewLetter()
        testManger.setTitle(title)
        testManger.setReceiver(receiver)
        testManger.setContent(from)
        testManger.sendLetter()
    }

    @After
    fun endTest() {
        driver.close()
    }
}